--Soma um valor inteiro a uma tupla contendo 2 inteiros. Saída é uma tupla com dois inteiros

soma :: (Int, Int) -> Int -> (Int, Int)
soma (a, b) n = (a+n, b+n)


--Faça uma função que calcula o resto da divisão

resto :: Int -> Int -> Int
resto a b
	| b == 0	= 0
	| otherwise	= mod a b


--Faça uma função que verifique se uma determinada letra é vogal

vogal :: Char -> Bool
vogal x = x == 'a' || x == 'e' || x == 'i' || x == 'o' || x == 'u' || x == 'A' || x == 'E' || x == 'I' || x == 'O' || x == 'U'

-- Seja o cadastro de pessoas dado pela função a seguir, contrua funções para mostrar a idade média das pessoas até um dado registro; a quantidade de homens; os dados de uma pessoa de maior idade; o nome de uma pessoa de idade menor que 18.

pessoa :: Float -> (String, Float, Char)
pessoa rg
	| rg == 1	= ("Joao Silva", 12 , 'm')
	| rg == 2	= ("Maria Santos", 12 , 'f')
	| rg == 3	= ("Jonas Souza", 12 , 'm')
	| rg == 4	= ("Silvia Cardoso", 12 , 'f')
	| rg == 5	= ("Jose Coelho", 12 , 'm')
	| rg == 6	= ("Manoel Pontes", 12 , 'm')
	| rg == 7	= ("Joao Carvalho", 12 , 'm')
	| rg == 8	= ("Marcia Santana", 12 , 'f')
	| rg == 9	= ("Cassia Martinez", 12 , 'f')
	| rg == 10	= ("Cassiano Silva", 12 , 'm')
	| otherwise	= ("Reg. Nao Encontrado" , 0 , 'i')


type Tpessoa = (String, Float, Char)

idade :: Tpessoa -> Float
idade (x, y, z) = y

--pegar a idade da pessoa com reg 5
--idade (pessoa 5)



-- Função que retorma a soma das idades dos n primeiros registros, onde n é parâmetro de entrada

somaIdadenRegistros :: Float -> Float
somaIdadenRegistros n
	|n == 1		= idade (pessoa 1)
	|otherwise	= idade (pessoa n) + somaIdadenRegistros(n-1)


-- Função que retorna a média das n idades

idadeMedia :: Float -> Float
idadeMedia n = (somaIdadenRegistros n) / n


--Função que devolve o sexo

sexo :: Tpessoa -> Char
sexo (x, y, z) = z


-- Função que retorma o numero de pessoas do sexo masculino

conta_masc :: Float -> Float
conta_masc n = masculino n 0


masculino :: Float -> Float -> Float
masculino n i
	|n == 0				= i
	|sexo (pessoa n) == 'm'		= masculino (n-1) i+1
	|otherwise			= masculino(n-1) i



