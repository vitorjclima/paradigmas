--Aula 31/10/2014
-- map
 

--Exercicio 1
--Faça uma função que calcule a seguinte série deTaylor (utilize folding)

--fatorial usando foldr1

--foldr1 (*) [1..10000]


taylor :: Float -> Float -> Float
taylor x n
	| n == 0	= 1
	| otherwise	= (x**n) / (foldr1 (*) [1..n]) + (taylor x (n-1))


--Exercicio 4
--Faça uma função que monte uma lista com a sequência de Collatz. Dado um número inteiro maior do que zero, e, se ele for par, dividimos por dois. Se for ímpar, multiplicamos por 3 e somamos 1. Aplique o mesmo processo ao resultado até que o número final obtido seja igual a 1. Coloque cada resultado em uma lista. Exemplo: collatz 13 [13, 40, 20, 10, 5, 16, 8, 4, 2, 1]

collatz x 
	| x == 1	= [1]
	| even x	= x : (collatz (div x 2))
	| odd x		= x : (collatz (x * 3 + 1))


--Exercicio 5
--Faça uma função que conte quantas sequências com tamanho maior do que um dado n temos entre um dado intervalo. Exemplo: contar quantas sequências de collatz maiores do que 15 temos no intervalo entre 1 e 100. numSeqCollatz 15 1 100

criaListSeq a b = map (collatz) [a..b]

numSeqCollatz n a b 
	| a > b || n == 0	= 0
	| otherwise		= length(filter(\ xs -> length xs > n)  (criaListSeq a b) )

--outro	| otherwise		= length(filter (> n) (map (length.collatz) [a..b]))





