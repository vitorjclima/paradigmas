--Aula 4 - 14/10/2014

--[exp | q1, q2, ..., qk]

--[x | x <- [-1..10], x>7]

--[(x,y) | x <- [1,2], y <- [3,4]]

--[(x,y) | (x,y) <- zip [1,2] [3,4]]

--zip forma tuplas, combina 1º com 1º, 2º com 2º, ...

--show mostra na tela strings e numeros, fazendo a conversao quando necessario

--read



--Soma os respectivos elementos de duas listas

--somaListas::[Int] –> [Int] –> [Int]
--somaListas xs ys = [x+y | (x,y) <– zip xs ys]

--uso: somaListas [1,2,3] [2,3,4,5]



--Obtém os números ímpares entre 50 e 70 de uma lista dada

--extrai :: [Int] –> [Int]
--extrai xs = [x | x <– xs, mod x 2 == 1, x >= 50, x <= 70]

--uso: extrai ([40..55] ++ [55, 58..90])


--Exercício 1:
--Usando compressão de listas, faça uma função que gere uma lista com as strings dos números binários de 4 bits

bin4 :: [String]
bin4 = [ [b1, b2, b3, b4] | b1 <- "01", b2 <- "01", b3 <- "01", b4 <- "01"]


--Exercício 2:
--Palavras com 4 caracteres, sem verificar se é válida

palavras4 :: [String]
palavras4 = [[b1, b2, b3, b4] | b1 <- ['a'..'l'], b2 <- ['a'..'l'], b3 <- ['a'..'l'], b4 <- ['a'..'l']]


--Exercício 3:
--Faça uma função que gere uma lista com as strings dos números binários de n bits.

binn :: Int -> [String]
binn n
	| n==1		= ["0", "1"]
	| otherwise	= [ x : y | x <- "01", y <- binn (n-1)]


--Exercício 4:
--Faça uma função que retira de uma lista de inteiros, todos múltiplos de n

retiraMultiploN :: Int -> [Int] -> [Int]
retiraMultiploN n list = [x | x <- list, mod x n /= 0]

--Exercício 5:
--Faça uma função que ordene os elementos de uma dada lista

menor [a] = a
menor (x : y)
	|x < (menor y)		= x
	|otherwise		= menor y

--Nesta função não precisa ser ter a linha de definição de tipo, pois é tipo generico.


removeMenor [a] = []
removeMenor (x : y)
	| x == menor ( x : y)	= y
	| otherwise 		= x : removeMenor y


ordena [] = []
ordena [a] = [a]
ordena lista = (menor lista) : ordena(removeMenor lista)

