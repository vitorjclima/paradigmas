/* Faca um predicado que descreva a seguinte gramatica

<expr> -> <termos> {(+|-) <termo>}
<termo> -> <fator> {(*|'/') <fator>}
<fator> -> <exp> { ** <exp>}
<exp> -> '(' <expr> ')' | <id>
<id> -> A | B | C | D

*/

expr --> termo, termo1.
termo1 --> "";("+";"-"), termo, termo1.
termo --> fator, fator1.
fator1 --> ""; ("*";"/"), fator, fator1.
fator --> exp, exp1.
exp1 --> "" ; "**", exp, exp1.
exp --> "(", expr, ")" ; id.
id --> "A";"B";"C";"D".


/*Representar, em Prolog, o problema de verificar se há um caminho entre duas cidades quaisquer calculando a distância entre elas. O predicado deve retornar a lista com o caminho e a distância total percorrida*/

conecta(saocarlos,araraquara,50).
conecta(saocarlos,jau,100).
conecta(saocarlos,bauru,170).
conecta(jau,bauru,20).
conecta(bauru,araraquara,130).
conecta(bauru,ribeiraobonito,114).
conecta(ribeiraobonito,araraquara,70).


caminho(X,Y,[X,Y],Z) :- conecta(X,Y,Z).
caminho(X,Y,[X|L],D) :- conecta(X,Z,D1), caminho(Z,Y,L,D2), D is D1+D2.


menorDist(X,Y,L,D) :- setof((D1,L1),caminho(X,Y,L1,D1),[(D,L)|_]).


/*Faca um predicado que ordene uma dada lista usando o insert sort*/

menorElem([H],H).
menorElem([H|T],M) :- menorElem(T,M1), (H < M1, M = H ; M =M1).

/*
ordena([],[]).
ordena([H|T],L) :- ordena(T,L1), insere(E,L1).

insere(X, [H|T],[H|T1]) :- X > Y, insere(X,L,L1), !.
insere(X,L,[X|L]).

*/











/*Dados 2 conjuntos representados por listas, faca um predicado que determina a interseccao dos dois conjuntos, tambem representado por lista

Ex: interseccao([1,2,3,4],[4,3,2,1],L).
L = [1,2,3,4].

interseccao([],[a,b,c],L).
L = [].

interseccao([c,d],[a,b,c],L).
L=[c].

*/
