/* Regras Gramaticais em Prolog*/

/* cabeca --> corpo. */

/*Executar este comando dentro do swipl antes de chamar este arquivo

           set_prolog_flag(double_quotes,chars). 
*/

reg --> ("a";"b";"c";"d"), "x".

bin --> ("0";"1") ; ("0";"1"), bin.

binfrac --> bin, (".", bin; "").

aba --> "a" , brep, "a".
brep --> ("";"b", brep).


ss --> "a" , ss , "b" ; "ab".


