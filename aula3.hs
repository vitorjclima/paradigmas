--Exercicio 1
-- Faça uma função que dada uma lista de inteiros retorne uma tupla com o primeiro e o último elemento da lista. Retorne (0,0) se a lista for vazia.

first_last_tupla :: [Int] -> (Int, Int)
first_last_tupla [] = (0,0)
first_last_tupla n = ( head n , last n )


first_last_tupla2 :: [Int] -> (Int, Int)
first_last_tupla2 n
	| n == []	= (0,0)
	| otherwise	= ( head n , last n )

--Exercício 2
--Faça uma função que dada uma string retorne a string sem o primeiro e último elemento

string_sem_primeiro_e_ultimo :: String -> String
string_sem_primeiro_e_ultimo [] = "Sting vazia"
string_sem_primeiro_e_ultimo n = tail (init n)

--Exercício 3
--Faça uma função que retorna o k-ésimo elemento de uma dada lista
--Exemplo: k_elemento[10,20,30,40] 2 >>> 20
--k_elemento "teste" 3 >>> 's'

k_elemento :: [generica] -> Int -> generica
k_elemento n i
	|i >= (length n)	= last n
	|otherwise 		= n!!(i-1)

--Haskell retornando mensagem de erro para solicitação de indices maiores ou menores que a lista passada.
k_elem :: [generica] -> Int -> generica
k_elem n i = n!!(i)


--Exercício 4
--Faça uma função que insira no final da lista um dado elemento, de forma que se o elemento já existir na lista ele não deverá ser inserido.

insere_elem ::(Eq a) => [a] -> a -> [a]
insere_elem n i
	|(elem i n)==True	= putStrLn "Elemento já presente na lista" --n
	|otherwise		= n++[i]



insere::(Eq a) => a -> [a] -> [a]
insere x y
	| y == [] = [x]					-- insere x [y:ys]
	| (head y) == x = y				-- | y == x = ys
	|otherwise = (head y) : insere x (tail y)	-- |otherwise = y: ( insere x ys)




