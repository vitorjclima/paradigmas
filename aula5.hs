--Exercício 1
--Defina uma função que retorne uma lista infinita de um inteiro dado.

inf :: Int -> [Int]
inf x = [x,x ..]


--Exercício 2
--Faça duas versões diferentes (uma usando break e outra usando span) para uma função que separe uma dada lista em duas partes a partir do primeiro número ímpar encontrado na lista.

separa :: [Int] -> ([Int],[Int])
separa lista = break odd lista

separa2 :: [Int] -> ([Int],[Int])
separa2 lista = span even lista


--Exercicio 4
--Faça uma função que retira a extensão de um dado nome de arquivo e outra que retorna a extensão desse arquivo

pegaExtensao :: String -> String
pegaExtensao ext = tail(dropWhile (/= '.') ext)

--Exercício 5
--Defina uma função que faça rotação à esquerda. O primeiro elemento da lista é inserido no final de uma lista (tipo polimórfico)

rotDireita lista = [last lista] ++ init lista

rotEsquerda lista = tail lista ++ [head lista]


--Exercício 6
--Defina uma função que dada uma lista gera todas as rotação à esquerda desta lista(polimórfico)

allRotEsq n lista | n == 0	= lista
		  | n == 1	= tail lista ++ [head lista]
		  | otherwise	= allRotEsq (n-1) (tail lista ++ [head lista])


--Exercicio 7
--Dado um inteiro x defina uma função que encontre todos os numeros entre 1000 e 1100 que não sejam multiplos de x

multiploX x = [lista | lista <- [1000..1100], mod lista x /= 0 ]

--Exercicio 8
--Faca uma funcao que remova todo n-esimo elemento de uma dada lista

eliminaN :: [a] -> Int -> [a]
eliminaN lista n = elimina lista n
	where
		elimina [] _ = []
		elimina (x:xs) 1 = elimina xs n
		elimina (x:xs) k = x:elimina xs(k-1)


eliminaN2 lista n
	|lista==[] 	= []
	|otherwise	= take (n-1) lista ++ eliminaN2 (drop n lista) n 


--Exercicio 9
--Faca uma funcao que dados 2 indices (i e j) e uma lista, ela retorne uma lista que vai do i-ésimo ao j-ésimo elemento

--criaListaij i j lista = takeWhile(lista!!j) (dropWhile (lista!!i) lista)

pedacoLista :: [a] -> Int -> Int -> [a]
pedacoLista lista i j
	| i > j		= []
	| otherwise	= take (j-i+1) (drop (i-1) lista)
