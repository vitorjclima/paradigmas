casou(joao, maria, dia(5, maio, 2000)).
casou(jose,claudia, dia(11,novembro,1950)).
casou(andre, fernanda, dia(11, agosto, 1960)).
casou(adriano, claudia, dia(15, outubro, 1973)).

/*
casou(_,maria,X).
casou(andre,_, dia(_,X,_)).
casou(adriano,_,dia(_,_,X)) , casou(Y,H,dia(_,_,Z)), Z =< X.
casou(A,B,dia(_,_,X)) , X >= 1984.
casou(A,B,dia(_,_,X)) , X =< 1990, X >= 1960.
*/



/* Fatorial Recursivo */

fatorial(X,1) :- X > 0 , X =:= 0 ; X =:= 1.
fatorial(X,R) :- X > 0 , X1 is (X-1) , fatorial(X1, R1), R is (R1*X).

/*mdc Recursivo*/





/* bicharada */
canario(piupiu).
peixe(nemo).
tubarao(tutu).
vaca(mimosa).
morcego(vamp).
avestruz(xica).
salmao(alfred).
peixes(X) :- peixe(X) ; tubarao(X) ; salmao(X).
passaros(X) :- canario(X) ; avestruz(X).
mamifero(X) :- vaca(X) ; morcego(X).
animal(X) :- peixes(X) ; passaros(X) ; mamiferos(X).
delicia(X) :- salmao(X).
cor(X,Y) :- canario(X), Y = amarelo.
grande(X) :- avestruz(X).
anda(X) :- avestruz(X) ; (mamifero(X), not(morcego(X))).
alimento(X) :- vaca(X) ; delicia(X).
pele(X) :- animal(X).
nadadeira(X) :- peixes(X).
nada(X) :- peixes(X).
asas(X) :- passaros(X) , not(avestruz(X)) ; morcego(X).
voa(X) :- passaros(X) , not(avestruz(X)) ; morcego(X).
ovos(X) :- passaros(X) ; peixes(X), not(tubarao(X)).
