/*Fatorial*/

fatorial(0,1). 

fatorial(N,F) :-
   N>0,
   N1 is N-1,
   fatorial(N1,F1),
   F is N * F1.

/*Ordenação de uma lista com o algoritmo insertion sort*/

isort([],[]).
isort([H|T],L) :- isort(T,T1), ins(H,T1,L).
ins(X,[],[X]).
ins(X,[Y|Ys],[Y|Zs]) :- X > Y, ins(X,Ys,Zs).
ins(X,[Y|Ys],[X,Y|Ys]) :- X =< Y.


/*Apaga todas as ocorrências de um dado elemento de uma lista*/

apaga([H|T],H,L) :- apaga(T,H,L).
apaga([H|T],X,[H|L]) :- H \== X, apaga(T,X,L).
apaga([],_,[]).


/* subtermo(T1,T2) testa se T1 é subtermo de T2 */

subtermo(T1,T2) :- T1 == T2.
subtermo(S,T) :- compound(T), functor(T,F,N), subtermo(N,S,T).
subtermo(N,S,T) :- N>1, N1 is N-1, subtermo(N1,S,T).
subtermo(N,S,T) :- arg(N,T,A), subtermo(S,A).


/*mínimo*/

minimo(X,Y,Y) :- X >= Y.
minimo(X,Y,X) :- X < Y.


/*progenitor*/

progenitor(A,B) :- pai(A,B).
progenitor(A,B) :- mae(A,B).
avo(X,Y) :- progenitor(X,Z), progenitor(Z,Y).
progenitor(A,B) :- pai(A,B) ; mae(A,B).
tio(X,Y) :- (pai(A,Y) ; mae(A,Y)), irmao(X,A).



/*Considere o seguinte código*/

amigo(ana,rui).
amigo(pedro,rui).
amigo(maria,helena).
amigo(pedro,ana).
amigo(maria,rui).
gosta(ana,cinema).
gosta(ana,pintura).
gosta(ana,ler).
gosta(rui,ler).
gosta(rui,musica).
gosta(maria,ler).
gosta(pedro,pintura).
gosta(pedro,ler).
compativeis(A,B) :- amigo(A,B), gosta(A,X), gosta(B,X).
compativeis(A,B) :- amigo(B,A), gosta(A,X), gosta(B,X).

/*Exemplos de utilização de findall, bagof e setof*/

/*
findall(X,compativeis(ana,X),L).
L = [rui,pedro,pedro]
*/


/*
bagof(X,compativeis(ana,X),L).
 L = [rui,pedro,pedro]
*/


/*
setof(X,compativeis(ana,X),L).
 L = [pedro,rui]
*/


/*Reverter uma lista*/

/*
reverse([1,3,5,7,9],X).
*/



/*ultimo elemento de uma lista*/
ultimo([Elem],Elem).
ultimo([_|Cauda],Elem) :- ultimo(Cauda,Elem).


/*soma dos elementos de uma lista*/

soma([],0).
soma([Elem|Cauda],S) :- soma(Cauda,S1), S is S1+Elem.


/*n-ésimo elemento de uma lista */

n_esimo(1,Elem,[Elem|_]).
n_esimo(N,Elem,[_|Cauda]) :- n_esimo(M,Elem,Cauda), Nis M+1.

/*numero de elementos de uma lista */

no_elem([],0).
no_elem([Elem|Cauda],N) :- no_elem(Cauda,N1), N is N1+1.

/*maior elemento de uma lista numerica*/

max([X,Y|Cauda],Max) :- X >= Y, !, max([X|Cauda],Max).
max([X,Y|Cauda],Max) :- max([Y|Cauda],Max). 
