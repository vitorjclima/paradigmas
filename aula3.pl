/*Exemplo de Lista*/

lista1([[], dead(z), [2, [b,c]], [], Z, [2, [b,c]]]).

/*Cabeça e Calda*/

lista1([X|Y]).


/*Concatenação de Listas*/

conc([ ], L, L).
conc(L, [ ], L).
conc( [X | L1], L2, [X | L3]) :- conc(L1, L2, L3).

/*Definir um predicado que faça a inserção de um elemento na primeira posição de uma lista*/

insere(X,L, [X|L]).

/*Definir um predicado que converta todos os valores de uma lista em seus valores absolutos*/

labs([],[]).
labs([C|L],[Y|L1]) :- abs(C,Y), labs(L,L1).


/*Escreva um predicado para encontrar o maior valor da lista*/

maior([C] , C).
maior([C|L] , Maior) :- maior(L, C1), (C >= C1 -> Maior = C ; Maior = C1).

/*Definir o predicado ultimo(Elem, Lista) que encontra o último elemento Elem de uma lista Lista.
- O último elemento de uma lista que tem somente um
elemento é o próprio elemento
- O último elemento de uma lista que tem mais de um elemento
é o último elemento da cauda*/

ultimo( listaVazia , []).
ultimo(Resp, [Resp]).
ultimo(Resp, [H|T]) :- ultimo(Resp,T).


/*Definir um predicado que faça a inserção de um elemento na n-esima posição de uma lista*/

/*inserePosN(Elem, [], _ , [Elem]).*/
inserePosN(Elem, L , 0 , [Elem|L]).
inserePosN(Elem, [H|T], Pos, [H|Resp1]) :- Pos1 is Pos-1, inserePosN(Elem, T, Pos1, Resp1).



