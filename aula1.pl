fruta(abacaxi).
fruta(banana).
fruta(goiaba).

igual(X,X).

/*Exercício 1*/
/*Usando a base ao lado, defina a regra: uma pessoa pode roubar algo se essa pessoa é um ladrão e ela gosta de um objeto.
Qual a resposta dada por Prolog à pergunta: João rouba o que? Como seria a pergunta?*/

ladrao(joao).
ladrao(pedro).
gosta(maria,comida).
gosta(maria,vinho).
gosta(joao,rubi).
gosta(joao,X):- gosta(X,vinho).

roubo(X,Y):- ladrao(X), gosta(X,Y).

/*Exercício 2*/

/* progenitor(X,Y) significa que X é progenitor de Y */
progenitor(pedro, carlos).
progenitor(pedro, ana).
progenitor(carlos,antonio).
progenitor(carlos,maria).
progenitor(joana,antonio).
progenitor(joana,maria).
progenitor(joana,ana).

/* definindo alguns fatos sobre o sexo */
homem(pedro).
homem(carlos).
homem(antonio).
mulher(ana).
mulher(maria).

/* pai(X,Y) significa que X é pai de Y */
pai(X,Y):-progenitor(X,Y) , homem(X).

/* mae(X,Y) significa que X é mãe de Y */
mae(X,Y):-progenitor(X,Y), mulher(X).

irma(X,Y) :- pai(P,X), pai(P,Y), mulher(Y), X \= Y.
irma(X,Y) :- mae(P,X), mae(P,Y), mulher(Y), X \= Y.

/*Exercício 3*/
/*Para a árvore genealógica do exemplo, defina as regras:
filho, filha
avo, avó
irmão, irmã
primo, prima
tio, tia
sobrinho, sobrinha*/


filho(X,Y) :- progenitor(Y,X), homem(X).
filha(X,Y) :- progenitor(Y,X), mulher(X).
filhos(X,Y) :- progenitor(X,Y).


avo(X,Y) :- pai(P,X), pai(Y,P).
avoh(X,Y) :- mae(P,X), mae(Y,P).

irma(X,Y) :- progenitor(P,X), progenitor(P,Y), mulher(Y), X \= Y.
irmao(X,Y) :- progenitor(P,X), progenitor(P,Y), homem(Y), X \= Y.

tio(X,Y) :- progenitor(P,Y), irmao(X,P).
tia(X,Y) :- progenitor(P,Y), irma(X,P).

primo(X,Y) :- homem(X), progenitor(T,X), progenitor(P,Y), irmao(T,P) ; irma(T,P).
prima(X,Y) :- mulher(X), progenitor(T,X), progenitor(P,Y), irmao(T,P) ; irma(T,P).

sobrinho(X,Y) :- tio(Y,X) ; tia(Y,X).
sobrinha(X,Y) :- tio(Y,X) ; tia(Y,X).
